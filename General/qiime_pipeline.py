#!/usr/bin/env python

from __future__ import division

__author__ = "Asker Brejnrod"
__copyright__ = "Copyright 2011, The QIIME project"
__credits__ = ["Asker Brejnrod"]
__license__ = "GPL"
__version__ = "1.5.0"
__maintainer__ = "Asker Brejnrod"
__email__ = "askerbrejnrod@gmail.com"
__status__ = "Release"
 

from os import makedirs
from qiime.parse import parse_qiime_parameters
from qiime.util import load_qiime_config, parse_command_line_parameters, make_option, get_options_lookup
import logging
import sys
import commands
import os
import time
import pprint
options_lookup = get_options_lookup()

asker_param_default = '/home/asker/data/parameter_files/default_strip_revprimers.txt'
chimera_reference='/home/asker/data/from_storage1_maxwell/Databases/gold.fa'
denoising_methods = {'mme_miseq': 'run_mme_miseq', 'denoiser': 'run_denoiser', 'ampliconnoise': 'run_ampliconnoise', 'acacia': 'run_acacia', 'split_libraries': 'run_split_libraries', 'none': 'prepare_fasta'}

script_info = {}
script_info['brief_description'] = "A workflow for denoising and basic sequence analyses"
script_info['script_description'] = ""
script_info['script_usage'] = [("","","")]
script_info['output_description']= ""
script_info['required_options'] = [\
 # Example required option
 #make_option('-i','--input_dir',type="existing_filepath",help='the input directory'),\
make_option('-i','--input_file',help='the input sequence [REQUIRED]'),\
make_option('-m','--mapping_file',type="existing_filepath",help='the input mapping file [REQUIRED]'),\
]
script_info['optional_options'] = [\
 # Example optional option
 #make_option('-o','--output_dir',type="new_dirpath",help='the output directory [default: %default]'),\
make_option('-o','--output_dir',type="new_dirpath",help='the output directory', default='analysis'),\
make_option('-p','--parameters_fp',type="existing_filepath",help='parameters file', default=asker_param_default),\
make_option('-c','--chimera_reference',type="existing_filepath",help='parameters file [default: %default]', default='/home/asker/data/from_storage1_maxwell/Databases/gold.fa'),\
make_option('-e','--email_adress',help='Your email adress'),\
make_option('-d','--denoising', type='choice', help='Select denoising? [default: %default]', choices = denoising_methods.keys(), default='denoiser'),\
make_option('-f','--force',action='store_true', dest='force', help='Force overwrite of existing output directory (note: existing files in output_dir will not be removed) [default: %default]'),
make_option('-r','--closed_reference',action='store_true', default=False, dest='closed_reference', help='Use closed reference picking protocol [default: %default]'),
options_lookup['jobs_to_start_workflow']
]
script_info['version'] = __version__

jobs_to_start = 10 
output_dir = 'analysis'
def run_denoiser(filename, mapping_file):
        log_and_execute("nice -n 19 sffinfo "+ filename +" > "+output_dir+"/temp.sff.txt")
        log_and_execute("nice -n 19 process_sff.py -i "+ filename + " -o "+output_dir+"/sff_out --use_sfftools")
        log_and_execute("nice -n 19 split_libraries.py -b 10 -m "+ mapping_file +" -f " +output_dir+ "/sff_out/*.fna -q "+output_dir+"/sff_out/*.qual -o "+output_dir+"/sl_out")
        log_and_execute("nice -n 19 denoise_wrapper.py --titanium  -i "+output_dir+"/temp.sff.txt -f "+output_dir+"/sl_out/seqs.fna -o "+output_dir+"/denoiser_denoised/ -m "+ mapping_file +" -n " + str(jobs_to_start))
        log_and_execute("nice -n 19 inflate_denoiser_output.py -c "+output_dir+"/denoiser_denoised/centroids.fasta -s "+output_dir+"/denoiser_denoised/singletons.fasta -f "+output_dir+"/sl_out/seqs.fna -d "+output_dir+"/denoiser_denoised/denoiser_mapping.txt -o "+output_dir+"/temp_denoised.fasta")
        log_and_execute("nice -n 19 usearch -uchime "+output_dir+"/temp_denoised.fasta -db "+chimera_reference+" -chimeras "+output_dir+"/temp_denoiser_chimeras.fasta -nonchimeras "+ output_dir + "/temp_denoiser_good.fasta")
        return("temp_denoiser_good.fasta")

def run_mme_miseq(filename, mapping_file):
        log_and_execute("mme_miseq_split_libraries.py -c BarcodeSequence -i " + filename+ " -m " + mapping_file + " -o "+output_dir+" -f"   )
        log_and_execute("nice -n 19 usearch -uchime "+output_dir+"/seqs.fasta -db "+chimera_reference+" -chimeras "+output_dir+"/temp_denoiser_chimeras.fasta -nonchimeras "+ output_dir + "/temp_miseq_good.fasta")
        return('temp_miseq_good.fasta')

def run_ampliconnoise(filename, mapping_file):
        log_and_execute("nice -n 19 sffinfo "+ filename +" > "+output_dir+"/temp.sff.txt")
        log_and_execute("nice -n 19 ampliconnoise.py -f --suppress_perseus -n "+ str(jobs_to_start)+" -i "+output_dir+"/temp.sff.txt -m " +mapping_file +" -o "+output_dir+"/temp_AN.fna --platform titanium")
        log_and_execute("nice -n 19 usearch -uchime "+output_dir+"/temp_AN.fna -db "+chimera_reference+" -chimeras "+output_dir+"/temp_AN_chimeras.fasta -nonchimeras "+output_dir+"/temp_AN_good.fasta")
        return('temp_AN_good.fasta')

def run_acacia(filename, mapping_file):
        makedirs(output_dir + '/acacia')
        log_and_execute("nice -n 19 process_sff.py -i "+ filename + " -o "+output_dir+"/sff_out --use_sfftools")
        log_and_execute("nice -n 19 split_libraries.py -b 10 -m "+ mapping_file +" -f " +output_dir+ "/sff_out/*.fna -q "+output_dir+"/sff_out/*.qual -o "+output_dir+"/sl_out")
        log_and_execute('java -jar /home/asker/Install/pbin//acacia-1.52.b0.jar -DFASTA_LOCATION='+ output_dir+ '/sl_out/seqs.fna -D OUTPUT_DIR=' + output_dir + '/acacia/' )
        log_and_execute("nice -n 19 usearch -uchime "+output_dir+"/acacia/acacia_out_all_tags.seqOut -db "+ chimera_reference + " -chimeras " +output_dir +"/temp_acacia_chimeras.fasta -nonchimeras "+output_dir+"/temp_acacia_good.fasta")

        return('temp_acacia_good.fasta')

def run_split_libraries(filename, mapping_file):
        log_and_execute("nice -n 19 process_sff.py -i "+ filename + " -o "+output_dir+"/sff_out --use_sfftools")
        log_and_execute("nice -n 19 split_libraries.py -b 10 -m "+ mapping_file +" -f " +output_dir+ "/sff_out/*.fna -q "+output_dir+"/sff_out/*.qual -o "+output_dir+"/sl_out")
        #log_and_execute("nice -n 19 split_libraries.py -b 10 -m "+ mapping_file +" -f " +output_dir+ "/sff_out/*.fna -q "+output_dir+"/sff_out/*.qual -o "+output_dir+"/sl_out")
        log_and_execute("nice -n 19 usearch -uchime "+output_dir+"/sl_out/seqs.fna -db "+chimera_reference+" -chimeras "+output_dir+"/temp_sl_chimeras.fasta -nonchimeras " +output_dir+ "/temp_sl_good.fasta")
        return("temp_sl_good.fasta")

def prepare_fasta(filename, mapping_file):
        log_and_execute("cp " + filename +" "+ output_dir + "/" + "clean_fasta.fasta")
        denoised_chimera_checked = "clean_fasta.fasta"
        return(denoised_chimera_checked)

def log_and_execute(command, abort=False):
        logger = logging.getLogger("qiime_pipeline")
        logger.info("Executing " + command)
        (status, output) = commands.getstatusoutput(command)
        logger.info("Got\n" + output)

def main():
        start_time = time.time()
        option_parser, opts, args = parse_command_line_parameters(**script_info)
        global chimera_reference
        chimera_reference = os.path.abspath(opts.chimera_reference)
        global output_dir
        output_dir = os.path.abspath(opts.output_dir)
        input_file = os.path.abspath(opts.input_file)
        parameters_fp = os.path.abspath(opts.parameters_fp)
        mapping_file = os.path.abspath(opts.mapping_file)
        global jobs_to_start
        jobs_to_start = opts.jobs_to_start
        try:
                makedirs(output_dir)
        except OSError:
                if opts.force:
                        pass
                else:
                    print "Output directory already exists. Please choose a different directory, or force overwrite with -f."
                #exit(1)
        if opts.parameters_fp:
                try:
                        parameter_f = open(opts.parameters_fp)
                except IOError:
                        raise IOError,\
                        "Can't open parameters file (%s). Does it exist? Do you have read access?"\
                        % opts.parameters_fp
                global params
                params = parse_qiime_parameters(parameter_f)
#                else:
#                        params = parse_qiime_parameters([])
        pp = pprint.PrettyPrinter(depth=6)
        pp.pprint(params)
        logger = logging.getLogger( 'qiime_pipeline')
        logger.setLevel(logging.DEBUG)
        fh = logging.FileHandler(opts.output_dir + '/qiime_pipeline.log')
        fh.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        if opts.verbose == True:
                ch = logging.StreamHandler()
                ch.setLevel(logging.DEBUG)
                ch.setFormatter(formatter)
                logger.addHandler(ch)

        logger.addHandler(fh)

        logger.info("starting " + sys.argv[0])
        logger.info("Denoising option selected: " + opts.denoising )

        fn = getattr(sys.modules[__name__], denoising_methods[opts.denoising])
        clean_file = fn(opts.input_file, opts.mapping_file)
        logger.info("Got clean file: " + clean_file)

        if opts.closed_reference == True:
                log_and_execute("nice -n 19 pick_reference_otus_through_otu_table.py -i  "  +output_dir+ "/" + clean_file + " -p " + parameters_fp + " -o "+ output_dir + " -f -r " + params['pick_otus']['reference_fp'] )
                log_and_execute("mkdir "  +output_dir+ "/otus; cp "  +output_dir+ "/uclust_ref_picked_otus/otu_table.biom "  +output_dir+ "/otus/otu_table.biom") 
        else:
                log_and_execute("nice -n 19 core_qiime_analyses.py -a  -i " +output_dir+ "/" + clean_file + " -o " + output_dir +" -m "+ mapping_file + " --suppress_split_libraries -p " + parameters_fp+" -f -O " + str(opts.jobs_to_start) )

        log_and_execute("convert_biom.py -i "+ output_dir +"/otus/otu_table.biom -o "+ output_dir + "/otus/otu_table_human_readable.txt -b --header_key taxonomy")
        log_and_execute("nice -n 19 summarize_taxa_through_plots.py -i " + output_dir + "/otus/otu_table.biom  -o "+ output_dir + "/taxa_summary -m " + mapping_file)
        log_and_execute("nice -n 19 sort_otu_table.py -i "+ output_dir + "/otus/otu_table.biom -o "+ output_dir + "/otus/otu_table_sorted.biom -m "+ mapping_file+" -s Description")
        log_and_execute("nice -n 19 summarize_taxa_through_plots.py -c Description  -i " + output_dir  +"/otus/otu_table_sorted.biom  -o "+ output_dir + "/taxa_summary_sorted_description -m " + mapping_file)
        log_and_execute("nice -n 19 make_otu_heatmap_html.py -i " + output_dir + "/otus/otu_table.biom -o "+ output_dir + "analysis/otu_heatmap")
        logger.info("Finished in " + str((time.time() - start_time)/60) + " minutes")

if __name__ == "__main__":
    main()

