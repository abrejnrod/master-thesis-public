#!/bin/bash


### TODO
#handles full paths -m poorly
#see if -f can be added to everything


### TODO
#handles full paths -m poorly - readlink everything
#see if -f can be added to everything

set -e #immediately exit script if any command fails
#defaults
AmpliconNoise=false 
Denoiser=false
Processors=1
UchimeReference=/home/asker/data/from_storage1_maxwell/Databases/gold.fa
Parameters=/home/asker/data/params.txt
Backup=false
while getopts badi:m:c:p:e:s: opt
do
    	case "$opt" in
      		a)  AmpliconNoise=true;;
		d)  Denoiser=true;;
		b)  Backup=true;;
      		i)  InputFile=`readlink -f "$OPTARG"`;;
		m)  MappingFile=`readlink -f "$OPTARG"`;;
		c)  Processors="$OPTARG";;
		p)  Parameters=`readlink -f "$OPTARG"`;;
		e)  Email="$OPTARG";;
		s)  Seqs=`readlink -f "$OPTARG"`;;
      		\?)		# unknown flag
      	  	echo >&2 \
	  	"usage: $0 [-c processors] [-a] [-i filename] [-m mapping file] [-e email notification] [-s fasta file]"
	  	exit 1;;
    	esac
done
shift `expr $OPTIND - 1`

date1=$(date +"%s")
if [ -z "$Seqs" ]; then

        if $Denoiser; then
                nice -n 19 sffinfo $InputFile > temp.sff.txt
		nice -n 19 process_sff.py -i ${InputFile} -o sff_out --use_sfftools
		nice -n 19 split_libraries.py -b 10 -m ${MappingFile} -f sff_out/*.fna -q sff_out/*.qual -o sl_out 
		nice -n 19 denoise_wrapper.py -v -i temp.sff.txt -f sl_out/seqs.fna -o denoiser_denoised/ -m  ${MappingFile} -n ${Processors} 
                nice -n 19 usearch -uchime denoiser_denoised/denoised_seqs.fasta -db ${UchimeReference} -chimeras temp_AN_chimeras.fasta -nonchimeras seqs_good.fasta 2> uchime.log 
	fi
	if $AmpliconNoise; then
		nice -n 19 sffinfo $InputFile > temp.sff.txt
		nice -n 19 ampliconnoise.py -n ${Processors} -i temp.sff.txt -m ${MappingFile} -o temp_AN.fna --platform titanium --suppress_perseus 
		nice -n 19 usearch -uchime temp_AN.fna -db ${UchimeReference} -chimeras temp_AN_chimeras.fasta -nonchimeras seqs_good.fasta 2> uchime.log
	else
		process_sff.py -i ${InputFile} -o sff_out --use_sfftools # TODO: check for availability
		split_libraries.py -b 10 -m ${MappingFile} -f sff_out/*.fna -q sff_out/*.qual -o sl_out #this is broken, -b 10 should be in a parameter file but split_libraries do no support that
		nice -n 19 usearch -uchime sl_out/seqs.fna -db ${UchimeReference} -chimeras seqs._chimeras.fasta -nonchimeras seqs_good.fasta 2> uchime.log

	fi
else
	if [ ! -f ./seqs_good.fasta ];
	then
		cp "$Seqs" ./seqs_good.fasta
	fi
fi

if $Backup; then
	mkdir ~/backup/`pwd|awk -F '/' ' { print ( $(NF) ) }'`
 	cp seqs_good.fasta ~/backup/`pwd|awk -F '/' ' { print ( $(NF) ) }'`
	cp ${MappingFile} ~/backup/`pwd|awk -F '/' ' { print ( $(NF) ) }'`
fi
				
#either way, seqs_good.fasta is used downstream 
nice -n 19 core_qiime_analyses.py -a  -i $PWD/seqs_good.fasta -o $PWD/analysis -m "$MappingFile" --suppress_split_libraries -O $Processors -p ${Parameters} -f
nice -n 19 sort_otu_table.py -i analysis/otus/otu_table.biom -o analysis/otus/otu_table_sorted.biom -m ${MappingFile} -s Description
nice -n 19 summarize_taxa_through_plots.py -i analysis/otus/otu_table.biom  -o analysis/taxa_summary -m ${MappingFile}
nice -n 19 summarize_taxa_through_plots.py -o analysis/taxa_summary_sorted -i analysis/otus/otu_table_sorted.biom -m ${MappingFile} -c Description -p ${Parameters}
nice -n 19 make_otu_heatmap_html.py -i analysis/otus/otu_table.biom -o analysis/otu_heatmap
convert_biom.py -i analysis/otus/otu_table.biom -o analysis/otus/otu_table_human_readable.txt -b --header_key taxonomy
date2=$(date +"%s")
diff=$(($date2-$date1))


SUBJECT="Analysis complete"
EMAILMESSAGE="/tmp/$$.emailmessage.txt"
echo "This is an automated notification that the QIIME pipeline is done. The following parameters were set"> $EMAILMESSAGE
echo "Path to analysis: ${PWD}/analysis" >> $EMAILMESSAGE
echo "AmpliconNoise: $AmpliconNoise" >> $EMAILMESSAGE
echo "Denoiser: $Denoiser" >> $EMAILMESSAGE
echo "Parameters: $Parameters" >> $EMAILMESSAGE
echo "Minutes spent: $((($date2-$date1) / (  60 ) ))" >> $EMAILMESSAGE 
mail -s "$SUBJECT" "$Email" < $EMAILMESSAGE
