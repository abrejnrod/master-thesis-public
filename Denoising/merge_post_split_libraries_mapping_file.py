#!/usr/bin/env python
# File created on 24 Jan 2013
from __future__ import division

__author__ = "Asker Brejnrod"
__copyright__ = "Copyright 2011, The QIIME project"
__credits__ = ["Asker Brejnrod"]
__license__ = "GPL"
__version__ = "1.5.0"
__maintainer__ = "Asker Brejnrod"
__email__ = "askerbrejnrod@gmail.com"
__status__ = "Release"
 

from cogent.parse.fasta import MinimalFastaParser
from qiime.util import parse_command_line_parameters, make_option
import os

script_info = {}
script_info['brief_description'] = ""
script_info['script_description'] = ""
script_info['script_usage'] = [("","","")]
script_info['output_description']= ""
script_info['required_options'] = [\
 # Example required option
 #make_option('-i','--input_dir',type="existing_filepath",help='the input directory'),\
make_option('-i','--input_seqs',help='the input sequences in post split_libraries format delimited by ,[REQUIRED]'),\
make_option('-m','--input_map',help='the input mapping files in the same order as the seqs files delimited by ,[REQUIRED]'),\
make_option('-o','--output_file',help='the output name. .fasta and .map will be appended [REQUIRED]'),\
]
script_info['optional_options'] = [\
 # Example optional option
 #make_option('-o','--output_dir',type="new_dirpath",help='the output directory [default: %default]'),\

make_option('-c','--column',help='Add a comma-delimited list of fields to the mapping file in the format Name:field1, field2 etc[REQUIRED]'),\
]
script_info['version'] = __version__



def main():
        option_parser, opts, args = parse_command_line_parameters(**script_info)

        if os.path.isfile(opts.output_file):
                puts("Output file already exists")
                exit(1)
        combined_fasta = open(opts.output_file+'.fasta', 'w')
        combined_map = open(opts.output_file+'.map', 'w')

        mapping_files = opts.input_map.split(",")        
        if opts.column != None and not len(mapping_files) == len(opts.column.split(':')[1].split(',')):
                print("Please input as many fields as mapping files")
                exit(1)

        for index, file in enumerate(opts.input_seqs.split(",")):
                for seq in MinimalFastaParser(open(file, 'r')):
                        print >>combined_fasta,'>' +str(index) + seq[0]
                        print >>combined_fasta,seq[1]
                for line in open(mapping_files[index]):
                        if line.find("#SampleID") != -1 and index == 0:
                                        if opts.column != None:
                                                temp = line.split("\t")
                                                temp.insert(-1, opts.column.split(':')[0])
                                                print >>combined_map,str(index) + "\t".join(temp),
                                                #print (line.strip() + "\t" + opts.column.split(':')[0] )
                                        else:
                                                print >>combined_map,line.strip()
                        elif line.find("#") == -1:
                                if opts.column != None:
                                        temp = line.split("\t")
                                        temp.insert(-1, opts.column.split(':')[1].split(',')[index])
                                        print >>combined_map,str(index) + "\t".join(temp),
                                        #print str(index) + line.strip() + "\t" + opts.column.split(':')[1].split(',')[index] 
                                else:
                                        print >>combined_map,str(index) + line.strip() 

        combined_fasta.close()
        combined_map.close()
if __name__ == "__main__":
    main()
