#/usr/bin/env python

import sqlite3
import md5
from scipy.stats import spearmanr

cor_run1_1 = range(1,6)
cor_run1_2 = range(1,7)
cor_run2 = [1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8]
cor_run3 = range(1,8)

conn = sqlite3.connect('/Users/abr/mariana_p/mariana_3588.db')

c = conn.cursor()

rows = c.execute('select otu_id,group_concat(count), group_concat(field_name), field_name, phylum, class, genus, sum(count) from otus where column_name = \'Description\' group by otu_id having sum(count) > 20 order by otu_id,field_name')

print "\t". join(["OTU", "Phylum", "Class", "Genus", "Correlation", "P", "Type", "Raw_data"])
for otu in rows:
    otu_id = otu[0]
    phylum = otu[4]
    classname = otu[5]
    genus = otu[6]
    ordered_run3 = map(float, otu[1].split(",")[27:])
    ordered_run2 = map(float, otu[1].split(",")[11:27])
    ordered_run1 = map(float, otu[1].split(",")[0:11])
    result_run3 = spearmanr(ordered_run3, cor_run3)
    result_run2 = spearmanr(ordered_run2, cor_run2)
    if 1:#result_run2[1] < 0.05 or result_run3[1] < 0.05:
        print str(otu_id) +"\t"+ phylum+"\t"+ classname+"\t"+ genus+"\t"+ str(result_run2[0])+"\t"+ str(result_run2[1])+"\t"+ "Amplicon" +"\t"+",".join(map(str,ordered_run2))
        print str(otu_id)+"\t"+ phylum+"\t"+ classname+"\t"+ genus+"\t"+ str(result_run3[0])+"\t"+ str(result_run3[1])+"\t"+ "Metagenome" + "\t"+ ",".join(map(str,ordered_run3))

conn.close()
