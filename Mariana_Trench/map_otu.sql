PRAGMA foreign_keys = ON;

BEGIN TRANSACTION;
CREATE TABLE otu_table(
otu_id integer not null,
sample_id integer not null,
count integer integer not null,
primary key (otu_id, sample_id)
);
CREATE TABLE classification(class_id integer not null,
otu_id integer not null, 
domain varchar(50),
phylum varchar(50),
class varchar(50),
orden varchar(50), 
family varchar(50),
genus varchar(50),
species varchar(50),
primary key (class_id)
);

CREATE TABLE column_name ( column_id integer not null,  
column_name varchar(256), 
primary key (column_id) 
);
CREATE TABLE map_table (map_id integer not null, 
sample_id integer not null, 
column_id integer not null,
field_name varchar(256), 
primary key(map_id)
);
CREATE TABLE sample_name (sample_id integer , 
sample_name varchar(256), 
primary key (sample_id) 
);
COMMIT;

create view otus as  select * from otu_table o join map_table m on (o.sample_id=m.sample_id) join column_name c on (c.column_id=m.column_id) join classification cl on (o.otu_id=cl.otu_id) join sample_name sn on (o.sample_id=sn.sample_id);