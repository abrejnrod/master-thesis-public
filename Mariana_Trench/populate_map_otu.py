#!/usr/bin/env python
#rm slet.db;cat map_otu.sql |sqlite3 slet.db
import sqlite3
import re
import argparse
import os

def create_db(filename):
	os.system("rm %s;cat map_otu.sql |sqlite3 %s" % (filename, filename))
	
def populate_mid_and_get_ids(line):
        mid_index = line.strip().split("\t")
        mid = [(x, ) for index, x in enumerate(mid_index) if not ( index == 0 or index == len(mid_index)-1 )]
        return exec_many("INSERT INTO sample_name (sample_name) VALUES (?)", mid )

#TODO
#def auto_detect_taxa_style(clean_taxa_string):
    #functions = {lambda (taxa_string): re.match(r'.*[k|p|c|o|f|g|s]__.*', taxa_string): insert_taxa_gg}
    #try:
        #functions[clean_taxa_string]
            
def insert_tax(taxa_string, otu_id):
    clean_taxa_string = "".join(taxa_string.split())
    return(insert_taxa_ggRDP(clean_taxa_string, otu_id))

def insert_taxa_ggRDP(taxa_string, otu_id):
    translate_gg = {1 : 'domain', 2: 'phylum', 3: 'class', 4: 'orden', 5: 'family', 6: 'genus', 7: 'species'}
    """ Greengenes specific version of insert_taxa()"""
    insert = [(index + 1 , x ) for index,x in enumerate(taxa_string.split(";"))]
    sql = "INSERT INTO classification (otu_id, %s) VALUES (%s)" % (",".join([translate_gg[x[0]] for x in insert]), ",".join(['?']*(len(insert)+1)))
    cursor = conn.cursor()
    cursor.execute(sql, [otu_id] + [empty_null(x[1]) for x in insert])
    conn.commit()
    return(cursor.lastrowid)

def insert_taxa_gg(taxa_string, otu_id):
    translate_gg = {'k' : 'domain', 'p': 'phylum', 'c': 'class', 'o': 'orden', 'f': 'family', 'g': 'genus', 's': 'species'}
    """ Greengenes specific version of insert_taxa()"""
    insert = [(x.split("__")[0], x.split("__")[1] ) for x in taxa_string.split(";")]
    sql = "INSERT INTO classification (otu_id, %s) VALUES (%s)" % (",".join([translate_gg[x[0]] for x in insert]), ",".join(['?']*(len(insert)+1)))
    cursor = conn.cursor()
    cursor.execute(sql, [otu_id] + [empty_null(x[1]) for x in insert])
    conn.commit()
    return(cursor.lastrowid)

def empty_null(string):
    if string == "":
        return 'NULL'
    else:
        return string

def exec_many(sql, data):
    cursor = conn.cursor()
    for d in data:
        cursor.execute(sql, d)
        conn.commit()
        yield cursor.lastrowid
        
def insert_otu_table(filename):    
    otu_table = open(filename, 'r')
    mid = []
    for line in otu_table:
        if line.startswith('#'):
            mid_indexes = list(populate_mid_and_get_ids(line))
            continue
        line_split = line.split("\t")
        otu_id = line_split[0]
        line_tax_id = insert_tax(line_split[-1], otu_id)
	#print mid_indexes, line_split
	for data in  zip(mid_indexes, enumerate(line_split[1:])):
            index = data[1][0]
	    #print data
            #if not ( index == 0 or index == len(line_split)-1 ):
	    if not ( index == len(line_split)-1 ):

                c = conn.cursor()
                sql = "INSERT INTO otu_table (otu_id, sample_id, count) VALUES (?,?,?)"
		#print sql, otu_id, mid_indexes[index], data[1][1]
                c.execute(sql, [otu_id, mid_indexes[index], data[1][1]])
        conn.commit()

def populate_column_name_and_get_ids(line):
        column_index = line.strip().split("\t")
        column = [(x, ) for index, x in enumerate(column_index) ]
        return exec_many("INSERT INTO column_name (column_name) VALUES (?)", column )
    
def get_sample_id_hash():
    c = conn.cursor()
    c.execute("select sample_id, sample_name from sample_name")
    return {x[1]: x[0] for x in c}

def insert_map_file(filename):
    column_indexes = []
    db_sample_ids = get_sample_id_hash()
    for line in open(filename, 'r'):
        line = line.strip()
        if line.startswith('#SampleID'):
            column_indexes = list(populate_column_name_and_get_ids(line))
            continue
        
        line_split = line.split("\t")
        sample_id = line_split[0]
        for data in zip(column_indexes, enumerate(line_split)):
            index = data[1][0]
            if not index == 0:
                c = conn.cursor()
                sql = "INSERT INTO map_table (sample_id, column_id, field_name) VALUES (?,?,?)"

                c.execute(sql, [db_sample_ids[sample_id], column_indexes[index], data[1][1]])
                conn.commit()
                
def get_args_parser():
    parser = argparse.ArgumentParser(description='Insert QIIME results into a database', epilog = 'Usage example: ')
    parser.add_argument('-o', '--output', help='Output file to write the database to')
    parser.add_argument('-b', '--base_dir', help='Base analysis dir for files')
    parser.add_argument('-m', '--mapping_file', help='Path to mapping file')
    return parser

def verify_base_dir_files(base_dir, rel_dict):
    for type, path in rel_dict.iteritems():
        if not os.path.isfile("%s/%s" % (base_dir, path)):
            raise Exception("The file of type %s, path %s doesnt exist" %(type, path))


def main():
    relative_paths= {'otu_table': 'otus/otu_table_human_readable.txt', 'rep_set': 'otus/rep_set/clean_fasta_rep_set.fasta'}
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", type=str,
                    help="Input OTU table in tabular format",required=True)
    parser.add_argument("-o", "--output", type=str,
                    help="Output file",required=True)
    parser.add_argument("-m", "--mapping_file", type=str,
                    help="Input mapping file",required=True)
    args = parser.parse_args()
    create_db(args.output)
    global conn
    conn = sqlite3.connect(args.output)
    
    #   if args.base_dir:
    #  verify_base_dir_files(args.base_dir, relative_paths)

    #insert_otu_table("%s/%s" % (args.base_dir, relative_paths['otu_table']))    
    insert_otu_table(args.input)
    insert_map_file(args.mapping_file)
    conn.close()


if __name__ == "__main__":
    main()

