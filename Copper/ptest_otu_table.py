#!/usr/bin/env python
#python ptest_otu_table.py -c Contamination -g C,U -i emtm_2011_otu_table_2086.txt -m emtm_2011_no_catheter.csv -o slet
from __future__ import division

import math
import random
import pylab as pl
import scipy.stats.mstats as ms
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", type=str,
                    help="Input OTU table in tabular format",required=True)
parser.add_argument("-o", "--output", type=str,
                    help="Output files basename",required=True)
parser.add_argument("-m", "--mapping_file", type=str,
                    help="Input mapping file",required=True)
parser.add_argument("-c", "--category", type=str,
                    help="Name of a column in the mapping file with the category to compare",required=True)
parser.add_argument("-g", "--groups", type=str,
                    help="The two groups to compare, eg -g Treatment,Control", required=True)
parser.add_argument("-d", "--differential", type=int,
                    help="Number of sequences that must be the difference between the two treatments", default=20)

args = parser.parse_args()

category = args.category
groups = args.groups.split(',')
differential = args.differential

if len(groups) != 2:
    raise Exception("Must test between two groups")


def relevant_lines(lines):
    for line in lines:
        if not line.startswith("#"):
            yield line
    
map_file = open(args.mapping_file)
category_index = map_file.readline().strip().split("\t").index(category)
map_file.seek(0)
tempr = {}
for x in relevant_lines(map_file):
    tempr[x.split("\t")[0]] = x.split("\t")[category_index]
mids = {x.split("\t")[0]: x.split("\t")[category_index] for x in relevant_lines(map_file)  }
otu_table = open(args.input, 'r')
mid_index = []
line_indices = []
kw_l = []
p_l = []
fc_l = []
otu_stat = []
neg_out = open(args.output + "_neg_otus.txt", 'w')
pos_out = open(args.output + "_pos_otus.txt", 'w')
all_out = open(args.output + "_all_otus.txt", 'w')

for line in otu_table:    
    if line.startswith('#OTU'):
        mid_index = line.strip().split("\t")
        for temp in groups:
            line_indices.append( [mid_index.index(index) for index in [k for k,v in mids.iteritems() if v == temp]])
        continue
    if line.startswith('#'):
        continue
    
    line_split = line.split("\t")
    otu_id = line_split[0]
    line_tax = line_split[-1].strip()
    group1 = [float(x) for index,x in enumerate(line_split) if index in line_indices[0] ]
    group2 = [float(x) for i,x in enumerate(line_split) if i in line_indices[1] ]
    if sum(group1) == 0 and sum(group2) == 0:
        continue
    kw = ms.kruskalwallis(group1,group2)
    kw = kw[1]
    fc = math.log(((sum(group1)+1)/len(group2)) / ((sum(group1) +1 ) /len(group2)),2)
    if abs(sum(group1) - sum(group2)) > differential and kw < 0.05:
        print >>all_out, otu_id, kw, fc, sum(group1), sum(group2), line_tax
        if fc < 0:
            print >>neg_out, otu_id
        if fc > 0:
            print >>pos_out, otu_id

