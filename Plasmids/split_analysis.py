#!/usr/bin/env python
# File created on 28 May 2013
from __future__ import division

__author__ = "Asker Brejnrod"
__copyright__ = "Copyright 2011, The QIIME project"
__credits__ = ["Asker Brejnrod"]
__license__ = "GPL"
__version__ = "1.5.0"
__maintainer__ = "Asker Brejnrod"
__email__ = "askerbrejnrod@gmail.com"
__status__ = "Release"
 


from qiime.util import parse_command_line_parameters, make_option
import sys
import commands
import tempfile
script_info = {}
script_info['brief_description'] = ""
script_info['script_description'] = ""
script_info['script_usage'] = [("","","")]
script_info['output_description']= ""
script_info['required_options'] = [\
 # Example required option
 #make_option('-i','--input_dir',type="existing_filepath",help='the input directory'),\
 make_option('-o','--output',help='Base of output name'),\
 make_option('-f','--input_fasta',help='Fasta file to split'),\
 make_option('-m','--mapping_file',help='Mapping file to split'),\
 make_option('-c','--column',help='column to split on'),\
 make_option('-g','--groups',help='Comma-separated groups to split on'),\
]
script_info['optional_options'] = [\
 # Example optional option
 #make_option('-o','--output_dir',type="new_dirpath",help='the output directory [default: %default]'),\
]
script_info['version'] = __version__


def relevant_lines(lines):
    for line in lines:
        if not line.startswith("#"):
            yield line


def main():
#for non-commandline running
#    sys.argv = ['/home/asker/Install/split_analysis.py', '-f', '/home/asker/data/analia_soil/analia_soil_analysis_fixed/clean_fasta.fasta', '-o', 'slet_test', '-m', '/home/asker/data/analia_soil/ptest_map.map', '-c', 'Treatment', '-g', 'U']
#    out_file = sys.stdout

    option_parser, opts, args =\
       parse_command_line_parameters(**script_info)
    out_file = open(opts.output + ".map", "w")
    #print script_info
    fasta_file = open(opts.input_fasta, "r")
    map_file = open(opts.mapping_file)
    category_index = map_file.readline().strip().split("\t").index(opts.column)
    map_file.seek(0)
    samples_to_retain = []
    for line in map_file:
        line = line.strip()
        if line.startswith("#"):
            print >>out_file,line
        elif line.split("\t")[category_index] == opts.groups:
            print >>out_file,line
            samples_to_retain.append(line.split("\t")[0])
    t = tempfile.NamedTemporaryFile()
    print  >>t, "\n".join(samples_to_retain)
    t.flush()
    command ="filter_fasta.py -f %s -o %s --sample_id_fp %s" % (opts.input_fasta, opts.output + ".fasta", t.name )
    commands.getoutput(command)
    t.close()
    out_file.close()


if __name__ == "__main__":
    main()
