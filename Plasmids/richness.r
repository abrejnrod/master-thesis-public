library(ggplot2)
nonparam_init <- c(0.1,2000)
nonparam_low <- c(0.01, 1)
nonparam_up <- c(0.999, 12000)
##
## GIGP
## 
gigp_init <- c(-1.326770, 0.0262937650, 0.2073401638)
gigp_low <- c(-4,10**-3,10**-5)
gigp_up <- c(0,0.6,0.8)

times = 2
opt_method="L-BFGS-B"

source("C:/Users/abr/Downloads/py/nonparam.r")
source("C:/Users/abr/Downloads/py/GIGP.r")
X <- read.table("C:/Users/abr/Downloads/py/fs312a.txt")

  x <- transcriptome(to_vector(X))
  N <- length(x)
  s <- length(unique(x))
  n <- c()
  k <- c()
  
  samp_size <- c(seq(N/2, N, by=10**4), N)
  
  for (i in samp_size) {
    samp <- as.vector(table(sample(x, i, replace=F)))
    n <- c(n, sum(samp))
    k <- c(k, length(samp))
  }
  
  m <- matrix(0, nrow=length(n), ncol=(times*2)+1) 
  m[,1] <- k
  rownames(m) <- n
  chaov <- vector()

  for (i in 1:times) {
    print(i)
    samp <- sample(x, N/times, replace=F)#no longer subsample N/1 istead N/2
    ##    samp_k <-  length(samp)
    ##    samp_N <-  sum(samp)
    
    # NONPARAM
    nonparam_samp <- sort(as.vector(table(samp)))
    samp_k <-  length(nonparam_samp)
    samp_N <-  sum(nonparam_samp)
    
    table <- table(samp)
    singletons <- dim(subset(as.data.frame(table), Freq == 1))[1]
    doubletons <- dim(subset(as.data.frame(table), Freq == 2))[1]
    s_obs <- length(unique(samp))
    chao1 <- s_obs + (singletons * (singletons - 1)) / (2 * doubletons + 1)
    print(c("CHAO1: ",chao1))
    chaov <- c(chaov, chao1)
    
    nonparam_opt <- optim(nonparam_init, nonparam_ll, NULL,  nonparam_samp, samp_k, samp_N, method=opt_method, control=list(fnscale=100000, parscale=c(1,1000)), lower=nonparam_low, upper=nonparam_up)
    print(c("NONPARAM", samp_k, samp_N))
    print(c("NONPARAM_OPT: ", nonparam_opt$par))
    print(c("NONPARAM_LL: ", nonparam_opt$value))#print value
    
    m[,i*2] <- nonparam_expected_k(nonparam_opt$par, samp_k, samp_N, samp_size-samp_N)
    
    
    # GIGP
    #gigp_samp <- FOF(samp)
    #print("GIGP")
    ##    gigp_opt <- optim(gigp_init, gigp_ll, NULL,  gigp_samp, samp_k, samp_N, method=opt_method, lower=gigp_low, upper=gigp_up)
    #gigp_opt <- optim(gigp_init, gigp_ll, NULL,  gigp_samp, samp_k, samp_N, method=opt_method, lower=gigp_low, upper=gigp_up,  control=list(parscale=c(0.1, 0.001, 0.001), trace=1))
    ##    gigp_opt <- optim(gigp_init, gigp_ll, NULL,  gigp_samp, samp_k, samp_N, method="Nelder-Mead",  control=list(parscale=c(0.1, 0.001, 0.001)))
    #print(c("GIGP_OPT: ", gigp_opt$par))
    #m[,i*2+1] <- gigp_expected_k(gigp_opt$par, get_S(gigp_opt$par, gigp_samp), samp_size)
  }
  
x_val <- as.integer(rownames(m))
plot(x_val, m[,2], ylim=c(1000,3000))
points(x_val, m[,1], col="red")
points(rep(tail(x_val, n=1), length(chaov)), chaov, col="green")

